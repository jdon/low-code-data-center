package com.yabushan.activiti.domain;

import java.util.List;
import java.util.Map;

import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Comment;

public class HistoryTaskInfo {
	/**
	 * 历史流程任务
	 */
	private HistoricTaskInstance historicTaskInstance;
	/**
	 * 任务变量
	 */
	private String info;

	private List<Comment> taskComment;
	private Map<String ,Object> varialbes;
	private Map<String ,Object> formVarialbes;
	public HistoricTaskInstance getHistoricTaskInstance() {
		return historicTaskInstance;
	}
	public void setHistoricTaskInstance(HistoricTaskInstance historicTaskInstance) {
		this.historicTaskInstance = historicTaskInstance;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public List<Comment> getTaskComment() {
		return taskComment;
	}
	public void setTaskComment(List<Comment> taskComment) {
		this.taskComment = taskComment;
	}


	public Map<String, Object> getVarialbes() {
		return varialbes;
	}

	public void setVarialbes(Map<String, Object> varialbes) {
		this.varialbes = varialbes;
	}

	public Map<String, Object> getFormVarialbes() {
		return formVarialbes;
	}

	public void setFormVarialbes(Map<String, Object> formVarialbes) {
		this.formVarialbes = formVarialbes;
	}
}
