package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubTreatise;
import com.yabushan.system.service.IEmpSubTreatiseService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工论文著作子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/treatise")
public class EmpSubTreatiseController extends BaseController
{
    @Autowired
    private IEmpSubTreatiseService empSubTreatiseService;

    /**
     * 查询员工论文著作子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:treatise:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubTreatise empSubTreatise)
    {
        startPage();
        List<EmpSubTreatise> list = empSubTreatiseService.selectEmpSubTreatiseList(empSubTreatise);
        return getDataTable(list);
    }

    /**
     * 导出员工论文著作子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:treatise:export')")
    @Log(title = "员工论文著作子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubTreatise empSubTreatise)
    {
        List<EmpSubTreatise> list = empSubTreatiseService.selectEmpSubTreatiseList(empSubTreatise);
        ExcelUtil<EmpSubTreatise> util = new ExcelUtil<EmpSubTreatise>(EmpSubTreatise.class);
        return util.exportExcel(list, "treatise");
    }

    /**
     * 获取员工论文著作子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:treatise:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubTreatiseService.selectEmpSubTreatiseById(recId));
    }

    /**
     * 新增员工论文著作子集
     */
    @PreAuthorize("@ss.hasPermi('system:treatise:add')")
    @Log(title = "员工论文著作子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubTreatise empSubTreatise)
    {
        return toAjax(empSubTreatiseService.insertEmpSubTreatise(empSubTreatise));
    }

    /**
     * 修改员工论文著作子集
     */
    @PreAuthorize("@ss.hasPermi('system:treatise:edit')")
    @Log(title = "员工论文著作子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubTreatise empSubTreatise)
    {
        return toAjax(empSubTreatiseService.updateEmpSubTreatise(empSubTreatise));
    }

    /**
     * 删除员工论文著作子集
     */
    @PreAuthorize("@ss.hasPermi('system:treatise:remove')")
    @Log(title = "员工论文著作子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubTreatiseService.deleteEmpSubTreatiseByIds(recIds));
    }
}
