package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubTraining;
import com.yabushan.system.service.IEmpSubTrainingService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工培训子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/training")
public class EmpSubTrainingController extends BaseController
{
    @Autowired
    private IEmpSubTrainingService empSubTrainingService;

    /**
     * 查询员工培训子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:training:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubTraining empSubTraining)
    {
        startPage();
        List<EmpSubTraining> list = empSubTrainingService.selectEmpSubTrainingList(empSubTraining);
        return getDataTable(list);
    }

    /**
     * 导出员工培训子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:training:export')")
    @Log(title = "员工培训子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubTraining empSubTraining)
    {
        List<EmpSubTraining> list = empSubTrainingService.selectEmpSubTrainingList(empSubTraining);
        ExcelUtil<EmpSubTraining> util = new ExcelUtil<EmpSubTraining>(EmpSubTraining.class);
        return util.exportExcel(list, "training");
    }

    /**
     * 获取员工培训子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:training:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubTrainingService.selectEmpSubTrainingById(recId));
    }

    /**
     * 新增员工培训子集
     */
    @PreAuthorize("@ss.hasPermi('system:training:add')")
    @Log(title = "员工培训子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubTraining empSubTraining)
    {
        return toAjax(empSubTrainingService.insertEmpSubTraining(empSubTraining));
    }

    /**
     * 修改员工培训子集
     */
    @PreAuthorize("@ss.hasPermi('system:training:edit')")
    @Log(title = "员工培训子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubTraining empSubTraining)
    {
        return toAjax(empSubTrainingService.updateEmpSubTraining(empSubTraining));
    }

    /**
     * 删除员工培训子集
     */
    @PreAuthorize("@ss.hasPermi('system:training:remove')")
    @Log(title = "员工培训子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubTrainingService.deleteEmpSubTrainingByIds(recIds));
    }
}
