package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubResume;
import com.yabushan.system.service.IEmpSubResumeService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工个人工作经历子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/resume")
public class EmpSubResumeController extends BaseController
{
    @Autowired
    private IEmpSubResumeService empSubResumeService;

    /**
     * 查询员工个人工作经历子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:resume:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubResume empSubResume)
    {
        startPage();
        List<EmpSubResume> list = empSubResumeService.selectEmpSubResumeList(empSubResume);
        return getDataTable(list);
    }

    /**
     * 导出员工个人工作经历子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:resume:export')")
    @Log(title = "员工个人工作经历子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubResume empSubResume)
    {
        List<EmpSubResume> list = empSubResumeService.selectEmpSubResumeList(empSubResume);
        ExcelUtil<EmpSubResume> util = new ExcelUtil<EmpSubResume>(EmpSubResume.class);
        return util.exportExcel(list, "resume");
    }

    /**
     * 获取员工个人工作经历子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:resume:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubResumeService.selectEmpSubResumeById(recId));
    }

    /**
     * 新增员工个人工作经历子集
     */
    @PreAuthorize("@ss.hasPermi('system:resume:add')")
    @Log(title = "员工个人工作经历子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubResume empSubResume)
    {
        return toAjax(empSubResumeService.insertEmpSubResume(empSubResume));
    }

    /**
     * 修改员工个人工作经历子集
     */
    @PreAuthorize("@ss.hasPermi('system:resume:edit')")
    @Log(title = "员工个人工作经历子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubResume empSubResume)
    {
        return toAjax(empSubResumeService.updateEmpSubResume(empSubResume));
    }

    /**
     * 删除员工个人工作经历子集
     */
    @PreAuthorize("@ss.hasPermi('system:resume:remove')")
    @Log(title = "员工个人工作经历子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubResumeService.deleteEmpSubResumeByIds(recIds));
    }
}
