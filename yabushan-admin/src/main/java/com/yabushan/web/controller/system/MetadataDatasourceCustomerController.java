package com.yabushan.web.controller.system;

import java.util.List;

import com.yabushan.common.annotation.DataScope;
import com.yabushan.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.MetadataDatasourceCustomer;
import com.yabushan.system.service.IMetadataDatasourceCustomerService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 用户注册数据源信息Controller
 *
 * @author yabushan
 * @date 2021-06-13
 */
@RestController
@RequestMapping("/system/metadatasourcecustomer")
public class MetadataDatasourceCustomerController extends BaseController
{
    @Autowired
    private IMetadataDatasourceCustomerService metadataDatasourceCustomerService;

    /**
     * 查询用户注册数据源信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:metadatasourcecustomer:list')")
    @DataScope(userAlias = "u.CREATE_ID")
    @GetMapping("/list")
    public TableDataInfo list(MetadataDatasourceCustomer metadataDatasourceCustomer)
    {
        startPage();
        String username = SecurityUtils.getUsername();
        if(!username.equals("admin")){
            metadataDatasourceCustomer.setCreateBy(username);
        }
        List<MetadataDatasourceCustomer> list = metadataDatasourceCustomerService.selectMetadataDatasourceCustomerList(metadataDatasourceCustomer);
        return getDataTable(list);
    }

    /**
     * 导出用户注册数据源信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:metadatasourcecustomer:export')")
    @Log(title = "用户注册数据源信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(MetadataDatasourceCustomer metadataDatasourceCustomer)
    {
        List<MetadataDatasourceCustomer> list = metadataDatasourceCustomerService.selectMetadataDatasourceCustomerList(metadataDatasourceCustomer);
        ExcelUtil<MetadataDatasourceCustomer> util = new ExcelUtil<MetadataDatasourceCustomer>(MetadataDatasourceCustomer.class);
        return util.exportExcel(list, "metadatasourcecustomer");
    }

    /**
     * 获取用户注册数据源信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:metadatasourcecustomer:query')")
    @GetMapping(value = "/{customerId}")
    public AjaxResult getInfo(@PathVariable("customerId") Long customerId)
    {
        return AjaxResult.success(metadataDatasourceCustomerService.selectMetadataDatasourceCustomerById(customerId));
    }

    /**
     * 新增用户注册数据源信息
     */
    @PreAuthorize("@ss.hasPermi('system:metadatasourcecustomer:add')")
    @Log(title = "用户注册数据源信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MetadataDatasourceCustomer metadataDatasourceCustomer)
    {
        return toAjax(metadataDatasourceCustomerService.insertMetadataDatasourceCustomer(metadataDatasourceCustomer));
    }

    /**
     * 修改用户注册数据源信息
     */
    @PreAuthorize("@ss.hasPermi('system:metadatasourcecustomer:edit')")
    @Log(title = "用户注册数据源信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MetadataDatasourceCustomer metadataDatasourceCustomer)
    {
        return toAjax(metadataDatasourceCustomerService.updateMetadataDatasourceCustomer(metadataDatasourceCustomer));
    }

    /**
     * 删除用户注册数据源信息
     */
    @PreAuthorize("@ss.hasPermi('system:metadatasourcecustomer:remove')")
    @Log(title = "用户注册数据源信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{customerIds}")
    public AjaxResult remove(@PathVariable Long[] customerIds)
    {
        return toAjax(metadataDatasourceCustomerService.deleteMetadataDatasourceCustomerByIds(customerIds));
    }
}
