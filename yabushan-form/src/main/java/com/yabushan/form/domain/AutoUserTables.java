package com.yabushan.form.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户建等级对象 auto_user_tables
 *
 * @author yabushan
 * @date 2021-08-06
 */
public class AutoUserTables extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 乐观锁 */
    private Long tId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long uId;

    /** VIP等级 */
    @Excel(name = "VIP等级")
    private String vipLevel;

    /** 建表数 */
    @Excel(name = "建表数")
    private Long allowTableNum;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private String updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    public void settId(Long tId)
    {
        this.tId = tId;
    }

    public Long gettId()
    {
        return tId;
    }
    public void setuId(Long uId)
    {
        this.uId = uId;
    }

    public Long getuId()
    {
        return uId;
    }
    public void setVipLevel(String vipLevel)
    {
        this.vipLevel = vipLevel;
    }

    public String getVipLevel()
    {
        return vipLevel;
    }
    public void setAllowTableNum(Long allowTableNum)
    {
        this.allowTableNum = allowTableNum;
    }

    public Long getAllowTableNum()
    {
        return allowTableNum;
    }
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("tId", gettId())
            .append("uId", getuId())
            .append("vipLevel", getVipLevel())
            .append("allowTableNum", getAllowTableNum())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .toString();
    }
}
