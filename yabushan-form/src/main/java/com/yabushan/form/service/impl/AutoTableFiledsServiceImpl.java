package com.yabushan.form.service.impl;

import java.util.List;

import com.yabushan.common.utils.DateUtils;
import com.yabushan.common.utils.SecurityUtils;
import com.yabushan.common.utils.StringUtils;
import com.yabushan.form.domain.AutoUserTable;
import com.yabushan.form.domain.AutoUserTables;
import com.yabushan.form.domain.Constant;
import com.yabushan.form.mapper.AutoUserTablesMapper;
import com.yabushan.form.mapper.FormInfosMapper;
import com.yabushan.form.service.IAutoUserTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.form.mapper.AutoTableFiledsMapper;
import com.yabushan.form.domain.AutoTableFileds;
import com.yabushan.form.service.IAutoTableFiledsService;

/**
 * 动态字段信息Service业务层处理
 *
 * @author yabushan
 * @date 2021-08-06
 */
@Service
public class AutoTableFiledsServiceImpl implements IAutoTableFiledsService
{
    @Autowired
    private AutoTableFiledsMapper autoTableFiledsMapper;

    @Autowired
    private IAutoUserTableService autoUserTablesService;
    @Autowired
    private FormInfosMapper formInfosMapper;
    @Autowired
    private AutoUserTablesMapper autoUserTablesMapper;

    /**
     * 查询动态字段信息
     *
     * @param fId 动态字段信息ID
     * @return 动态字段信息
     */
    @Override
    public AutoTableFileds selectAutoTableFiledsById(String fId)
    {
        return autoTableFiledsMapper.selectAutoTableFiledsById(fId);
    }

    /**
     * 查询动态字段信息列表
     *
     * @param autoTableFileds 动态字段信息
     * @return 动态字段信息
     */
    @Override
    public List<AutoTableFileds> selectAutoTableFiledsList(AutoTableFileds autoTableFileds)
    {
        return autoTableFiledsMapper.selectAutoTableFiledsList(autoTableFileds);
    }

    /**
     * 新增动态字段信息
     *
     * @param autoTableFileds 动态字段信息
     * @return 结果
     */
    @Override
    public int insertAutoTableFileds(AutoTableFileds autoTableFileds)
    {
        //校验表是否存在
        AutoUserTable autoUserTable1 = autoUserTablesService.selectAutoUserTableById(autoTableFileds.getbId());
        if(autoUserTable1!=null && autoUserTable1.getCreatedBy().equals(SecurityUtils.getUsername())){
            //表存在
            AutoTableFileds param = new AutoTableFileds();
            param.setbId(autoTableFileds.getbId());
            int size = autoTableFiledsMapper.selectAutoTableFiledsList(param).size();
            AutoUserTables autoUserTables=new AutoUserTables();
            autoUserTables.setCreatedBy(SecurityUtils.getUsername());
            AutoUserTables autoUserTables1 = autoUserTablesMapper.selectAutoUserTablesList(autoUserTables).get(0);
            //判断表是否超字段
            if(autoUserTables1.getVipLevel().equals("南瓜籽")){
                if(size>= Constant.NGZ.getONE_TABLE_FILED_NUM()){
                    return 100;//超过最大建表数
                }
            }else if(autoUserTables1.getVipLevel().equals("南瓜苗")){
                if(size>= Constant.NGM.getONE_TABLE_FILED_NUM()){
                    return 100;//超过最大建表数
                }
            }else if (autoUserTables1.getVipLevel().equals("南瓜藤")){
                if(size>= Constant.NGT.getONE_TABLE_FILED_NUM()){
                    return 100;//超过最大建表数
                }
            }else if(autoUserTables1.getVipLevel().equals("南瓜花")){
                if(size>= Constant.NGH.getONE_TABLE_FILED_NUM()){
                    return 100;//超过最大建表数
                }
            }else if(autoUserTables1.getVipLevel().equals("南瓜树")){
                if(size>= Constant.NGS.getONE_TABLE_FILED_NUM()){
                    return 100;//超过最大建表数
                }
            }else{
                return 100;//超过最大建表数
            }

            autoTableFileds.setfId(StringUtils.getUUID());
            autoTableFileds.setFiledEnNname(SecurityUtils.getUsername()+"_"+DateUtils.dateTimeNow());
            autoTableFileds.setCreatedBy(SecurityUtils.getUsername());
            autoTableFileds.setCreatedTime(DateUtils.getNowDate());
            StringBuffer SQLbuffer = new StringBuffer();
            SQLbuffer.append("ALTER TABLE  ").append(autoUserTable1.getbEnName())
                    .append("  ADD  ").append(autoTableFileds.getFiledEnNname())
                    .append(" ").append(autoTableFileds.getFiledType());

            //alter增加表字段
            formInfosMapper.dynamicsCreate(SQLbuffer.toString());


            return autoTableFiledsMapper.insertAutoTableFileds(autoTableFileds);
        }else {
            //表不存在
            return -1;
        }

    }

    /**
     * 修改动态字段信息
     *
     * @param autoTableFileds 动态字段信息
     * @return 结果
     */
    @Override
    public int updateAutoTableFileds(AutoTableFileds autoTableFileds)
    {
        return autoTableFiledsMapper.updateAutoTableFileds(autoTableFileds);
    }

    /**
     * 批量删除动态字段信息
     *
     * @param fIds 需要删除的动态字段信息ID
     * @return 结果
     */
    @Override
    public int deleteAutoTableFiledsByIds(String[] fIds)
    {
        return autoTableFiledsMapper.deleteAutoTableFiledsByIds(fIds);
    }

    /**
     * 删除动态字段信息信息
     *
     * @param fId 动态字段信息ID
     * @return 结果
     */
    @Override
    public int deleteAutoTableFiledsById(String fId)
    {
        return autoTableFiledsMapper.deleteAutoTableFiledsById(fId);
    }
}
